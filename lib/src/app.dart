import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/seat_booking_bloc.dart';
import 'constant/global_constant.dart';
import 'screen/seat_booking_screen.dart';

class App extends StatelessWidget {
  const App({super.key});

  @override
  Widget build(BuildContext context) {
    final seatBookingBloc = SeatBookingBloc();

    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => seatBookingBloc,
        )
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: GlobalConstant.appName,
        theme: ThemeData(
          colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
          useMaterial3: true,
        ),
        home: const SeatBookingScreen(),
      ),
    );
  }
}
