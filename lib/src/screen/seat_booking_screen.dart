import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:two_dimensional_scrollables/two_dimensional_scrollables.dart';

import '../bloc/seat_booking_bloc.dart';
import '../bloc/seat_booking_event.dart';
import '../bloc/seat_booking_state.dart';
import '../constant/screen_constant.dart';
import '../model/booked_seat.dart';
import '../widget/time_scale.dart';

class SeatBookingScreen extends StatelessWidget {
  const SeatBookingScreen({super.key});

  @override
  Widget build(BuildContext context) {
    List<BookedSeat> bookedSeats = [];

    return SafeArea(
      child: Scaffold(
        body: BlocBuilder<SeatBookingBloc, SeatBookingState>(
            builder: (context, state) {
          if (state is SeatBookingSuccess) {
            bookedSeats = state.bookedSeats;
            context.read<SeatBookingBloc>().add(SeatBookingRefreshed());
          }

          return TableView.list(
            pinnedColumnCount: 1,
            pinnedRowCount: 1,
            columnBuilder: (int index) {
              return TableSpan(
                extent: index == 0
                    ? const FixedTableSpanExtent(70)
                    : const FixedTableSpanExtent(100),
              );
            },
            rowBuilder: (int index) {
              return TableSpan(
                extent: index == 0
                    ? const FixedTableSpanExtent(30)
                    : const FixedTableSpanExtent(40),
              );
            },
            cells: List.generate(
              31,
              (row) => List.generate(
                25,
                (column) {
                  List<int> timeScales = [];
                  List<List<int>> timeRanges = [];

                  if (column == 0 && row == 0) {
                    return DecoratedBox(
                      decoration: const BoxDecoration(
                        color: Colors.greenAccent,
                        border: Border(
                          bottom: BorderSide(),
                          right: BorderSide(),
                        ),
                      ),
                      child: CustomPaint(
                        painter: RectanglePainter(),
                        child: Stack(
                          children: [
                            Align(
                              alignment: Alignment.topRight,
                              child: Padding(
                                padding: const EdgeInsets.all(4.0),
                                child: Text(
                                  SeatBookingConstant.time,
                                  style: Theme.of(context).textTheme.bodySmall,
                                ),
                              ),
                            ),
                            Align(
                              alignment: Alignment.bottomLeft,
                              child: Padding(
                                padding: const EdgeInsets.all(4.0),
                                child: Text(
                                  SeatBookingConstant.seat,
                                  style: Theme.of(context).textTheme.bodySmall,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                  }

                  if (column == 0) {
                    return DecoratedBox(
                      decoration: const BoxDecoration(
                        color: Colors.greenAccent,
                        border: Border(
                          bottom: BorderSide(),
                          right: BorderSide(),
                        ),
                      ),
                      child: Center(
                        child: Text(
                          '${SeatBookingConstant.seatNum} $row',
                          style: Theme.of(context).textTheme.bodySmall,
                        ),
                      ),
                    );
                  }

                  if (row == 0) {
                    return TimeScale(
                      time: '${column - 1}${SeatBookingConstant.timeSufix} ',
                      color: Colors.greenAccent,
                    );
                  }

                  if (bookedSeats.isNotEmpty) {
                    for (BookedSeat bookedSeat in bookedSeats) {
                      if (row == bookedSeat.seat) {
                        List<int> bookedScales = bookedSeat.bookedScales;
                        if (bookedScales.contains(column)) {
                          timeRanges = bookedSeat.timeRanges;
                          timeScales = timeRanges
                              .elementAt(bookedScales.indexOf(column));
                        }
                      }
                    }
                  }

                  return Row(
                    children: List.generate(
                      12,
                      (index) {
                        return Expanded(
                          flex: 1,
                          child: DecoratedBox(
                            decoration: BoxDecoration(
                              color: timeScales.contains(index)
                                  ? Colors.greenAccent
                                  : null,
                              border: Border(
                                bottom: const BorderSide(),
                                right: index == 11
                                    ? !timeScales.contains(index)
                                        ? const BorderSide(width: 0.5)
                                        : BorderSide.none
                                    : BorderSide.none,
                                left: index == 0
                                    ? !timeScales.contains(index)
                                        ? const BorderSide(width: 0.5)
                                        : BorderSide.none
                                    : BorderSide.none,
                              ),
                            ),
                            child: GestureDetector(
                              onTap: () {
                                context.read<SeatBookingBloc>().add(SeatBooked(
                                      seatNumber: row,
                                      tapIndex: index,
                                      timeScaleNumber: column,
                                    ));
                              },
                            ),
                          ),
                        );
                      },
                    ),
                  );
                },
              ),
            ),
          );
        }),
      ),
    );
  }
}

class RectanglePainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final crossLine = Paint()
      ..color = Colors.black
      ..strokeWidth = 1.0;

    canvas.drawLine(Offset.zero, Offset(size.width, size.height), crossLine);
  }

  @override
  bool shouldRepaint(RectanglePainter oldDelegate) => false;
}
