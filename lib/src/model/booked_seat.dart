import 'package:equatable/equatable.dart';

class BookedSeat extends Equatable {
  final int seat;
  final List<int> bookedScales;
  final List<List<int>> timeRanges;

  const BookedSeat({
    required this.seat,
    required this.bookedScales,
    required this.timeRanges,
  });

  @override
  List<Object?> get props => [
        seat,
        bookedScales,
        timeRanges,
      ];
}
