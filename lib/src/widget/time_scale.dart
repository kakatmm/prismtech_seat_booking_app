import 'package:flutter/material.dart';

class TimeScale extends StatelessWidget {
  const TimeScale({
    super.key,
    this.time = '',
    this.color = Colors.transparent,
  });

  final String time;
  final Color color;

  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
      decoration: BoxDecoration(
        color: color,
        border: const Border(
          bottom: BorderSide(),
          right: BorderSide(),
        ),
      ),
      child: Column(
        children: [
          Expanded(
            flex: 1,
            child: Align(
              alignment: Alignment.topLeft,
              child: Text(
                time,
                style: Theme.of(context).textTheme.bodySmall,
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Row(
              children: List.generate(
                12,
                (index) => Expanded(
                  flex: 1,
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: Container(
                      height: ++index % 6 == 0
                          ? null
                          : index % 3 == 0
                              ? 11
                              : 8,
                      decoration: const BoxDecoration(
                        border: Border(right: BorderSide()),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
