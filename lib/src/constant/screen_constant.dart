class SeatBookingConstant {
  static String time = 'Time';
  static String seat = 'Seat';
  static String timeSufix = ':00';
  static String seatNum = 'No.';
}
