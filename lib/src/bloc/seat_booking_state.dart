import 'package:equatable/equatable.dart';

import '../model/booked_seat.dart';

abstract class SeatBookingState extends Equatable {
  @override
  List<Object?> get props => [];
}

class SeatBookingInitial extends SeatBookingState {}

class SeatBookingSuccess extends SeatBookingState {
  final List<BookedSeat> bookedSeats;

  SeatBookingSuccess({
    required this.bookedSeats,
  });

  @override
  List<Object?> get props => [
        bookedSeats,
      ];
}

class SeatBookingFailure extends SeatBookingState {}
