import 'package:bloc/bloc.dart';

import '../model/booked_seat.dart';
import 'seat_booking_event.dart';
import 'seat_booking_state.dart';

class SeatBookingBloc extends Bloc<SeatBookingEvent, SeatBookingState> {
  final List<BookedSeat> bookedSeats = [];

  SeatBookingBloc() : super(SeatBookingInitial()) {
    on<SeatBooked>((event, emit) {
      int totalMinute = (12 * event.timeScaleNumber + event.tapIndex) * 5;
      int hour = (totalMinute ~/ 60) - 1;
      int minute = totalMinute % 60;

      List<List<int>> timeRanges = [];
      List<int> timeScales = [];
      int timeScale = event.timeScaleNumber;
      int fisrtTapIndex = event.tapIndex;
      int maxRange = 0;
      List<int> subTimeRange = [];

      while (maxRange < 24) {
        for (int i = fisrtTapIndex; i < 12; i++) {
          subTimeRange.add(i);
          maxRange++;
          if (maxRange == 24) {
            break;
          }
        }
        timeScales.add(timeScale);
        timeRanges.add([...subTimeRange]);
        timeScale++;
        fisrtTapIndex = 0;
        subTimeRange.clear();
      }

      bookedSeats.add(BookedSeat(
        seat: event.seatNumber,
        bookedScales: timeScales,
        timeRanges: timeRanges,
      ));
      print('booked seats: $bookedSeats');
      emit(SeatBookingSuccess(bookedSeats: bookedSeats));
    });

    on<SeatBookingRefreshed>((event, emit) {
      emit(SeatBookingInitial());
    });
  }
}
