import 'package:equatable/equatable.dart';

abstract class SeatBookingEvent extends Equatable {
  @override
  List<Object?> get props => [];
}

class SeatBooked extends SeatBookingEvent {
  final int tapIndex;
  final int seatNumber;
  final int timeScaleNumber;

  SeatBooked({
    required this.tapIndex,
    required this.seatNumber,
    required this.timeScaleNumber,
  });

  @override
  List<Object?> get props => [
        tapIndex,
        seatNumber,
        timeScaleNumber,
      ];
}

class SeatBookingRefreshed extends SeatBookingEvent {}
